[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/MadeByThePinsTeam-DevLabs/RecapTime-Staff/status-reports) [![LICENSE](https://img.shields.io/badge/license-Anti%20996-blue.svg)](https://github.com/996icu/996.ICU/blob/master/LICENSE)

# Status Reports from Recap Time Department
This repoistory backs our Incident Tracker (https://incident-tracker.madebythepins.tk). The status page is generated statically and pushed to GitLab for the CI to build and publish it.
Our website's CDN and DDoS protection are managed by Cloudflare.

## Documentation for Staff
Since this GitLab project is **GitPod Ready-to-Code** cerifitied, we recommend to open this in GitPod.

[![Open in Gitpod](http://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/MadeByThePinsTeam-DevLabs/RecapTime-Staff/status-reports)

### Incident Creation
Run `yarn run tracker:new-incident` and fill up the required data. See the README in the `content` directory
for details in editing incidents.

Before pushing to GitLab, preview it with `yarn run gl-pages:local-preview`.

If everything is good, push with `yarn run git:publish` and allow GitLab CI to build it for you.

### Managing Incidents
Either run `yarn run tracker:update-incident` to just update the basic incident info or edit the Markdown file of the incident you want to edit.

To remove, run `yarn run tracker:delete-incident` and follow prompts. It's not recommended to delete incidents without having to add to the `archives` directory.

## Feedback?
Your feedback is appericated in improving our Incident Tracker. [Use our GitLab Issue Tracker](#https://gitlab.com/MadeByThePinsTeam-DevLabs/RecapTime-Staff/status-reports/issues)
to send your feedback.

Merge requests are also welcome!

## License
Our Incident Tracker source code is licensed under [Anti-996 License](LICENSE) in effort to support Anti-996 protest.

To learn more, visit <https://996.icu>.