---
title: COVID-19 Pandemic Tracker
date: '2020-02-01T00:00:00.000Z'
severity: major-outage
description:  "This is where we track the pandemic situtation on COVID-19."
affectedsystems:
  - other-things-we-tracking
  - pandemic-tracker
resolved: false
---

::: update monitoring | 2020-03-20T18:36:40.500Z
As of 19 March 2020, 17:27 GMT+8 from the situation dashboard, the COVD-19 disease had...

| Confirmed Cases | Deaths | Countries Affected |
| --------------- | ------ | ------------------ |
| 209,839 | 8,778 | 169 |
:::

::: update monitoring | 2020-03-22T17:34:40.500Z
As of 22 March 2020, 17:27 GMT+8 from the situtation dashboars, the COVID-19 disease had...

| Confirmed Cases | Death | Countries Affected |
| 267,013 | 11,201 | 184 |

Compared to the data from 4 days ago, **57174 new confirmed cases, 2423 deaths and 15 
:::

## Where do you get those updates?
Our sources in making the pandemic tracker are listed as follows below.

* [WHO Situtation Dashboard](https://experience.arcgis.com/experience/685d0ace521648f8a5beeeee1b9125cd) for numbers.

<!--- language code: en -->
