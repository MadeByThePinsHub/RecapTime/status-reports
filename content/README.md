# Incident Tracker Content

## Reminders for project devs and maintainers
All files should be `incident-name-here.md`, not `YYYY-MM-DD_incident-name-here.md`. After you created a new incident,
remove the `YYYY-MM-DD_` part in these files.

## About this folder
This directory contains your Application Incidents.

More information about the usage of this directory in [the documentation](https://docs.statusfy.co).
