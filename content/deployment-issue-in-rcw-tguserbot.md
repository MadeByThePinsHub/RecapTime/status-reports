---
title: An deployment issue was triggered on Retarded Cases Watcher
date: '2020-07-01T19:14:30.224Z'
severity: 'major-outage'
affectedsystems:
    - api
    - telegram-userbots
resolved: false
---

Our 20th deployment of our RCW userbot to Heroku went rekt due to some issues relating to our MongoDB URL.

::: update Applying some fixes | 2020-07-02T16:48:12.000Z
We're gona add `dnspython` to our dependencies and also trying the 3.4+ format.
:::

::: update Found the issue | 2020-07-01T20:13:30.666Z 
We found the issue and that's we used the 3.6+ MongoDB URL (which requires `dnspython`) instead of the 3.4+ one.
:::


::: update Investigating | 2020-07-01T19:14:30.224Z
After deploying our code to Heroku, we inspect the process logs to check if anything is working.
:::
