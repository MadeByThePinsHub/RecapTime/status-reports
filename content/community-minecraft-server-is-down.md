---
title: Community Minecraft Server is down
date: '2020-04-03T17:15:33.366Z'
severity: major-outage
affectedsystems:
  - minecraft-servers
resolved: false
---

::: update Starting soon | 2020-04-03T18:12:45.500Z
Due to coronavirus outbreak and the long queueing times in Aternos, we'll start our Community Server up soon on the times where the queueing times is short.
:::

::: update Editing the configuration file is finished | 2020-04-03T17:46:45.500Z
We're been done editing those files, and our server is on the queue to start. We'll montioring what's next.
:::

::: update The server is currently down | 2020-04-03T17:16:40.500Z
There is issue in plugin configuration for `AuthMeReloaded`, which causes the server to crashed and shuts down. We'll update you about this soon.
:::

## Need more details about this?
* See the logs: <https://mclo.gs/KL5VphW>

<!--- language code: en -->
