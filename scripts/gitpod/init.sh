#!/bin/sh
## Update Node Version Manager
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

## Install Node.js with Node Version Manager then update npm
nvm install 12.16

## Then, install dependencies
npm install -g
